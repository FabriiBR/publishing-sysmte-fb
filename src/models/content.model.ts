import {Entity, model, property} from '@loopback/repository';

@model()
export class Content extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: false,
    required: true,
  })
  id: number;

  @property({
    type: 'string',
  })
  content?: string;

  @property({
    type: 'number',
  })
  reaction?: number;

  @property({
    type: 'number',
  })
  user_id?: number;

  @property({
    type: 'date',
  })
  created_at?: string;


  constructor(data?: Partial<Content>) {
    super(data);
  }
}

export interface ContentRelations {
  // describe navigational properties here
}

export type ContentWithRelations = Content & ContentRelations;
